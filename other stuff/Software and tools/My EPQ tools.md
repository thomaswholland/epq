# My EPQ tools

I have recently looked at all the possible tools that are available for me to use when I am doing my EPQ as well as my A levels. At the core of my search were these criteria:

1. Try to use as far as possible open source software as I believe heavily in this philosophy.
2. For it to fit to my work flow on my Mac and iPad
3. For me to be able to pick up where I left off no mater where I am, and what device I am using
4. For it to have some method of version control as this will allow me to see my progress and see any changes



1. For my bookmarks and information capture, I am going to be using a tool called [Zotero](https://www.zotero.org) and this is an online tool that has a Mac application and allows for indepth collation of not only web pages but also pdfs, books and any document. It is open source.
2. For my writing, I am planning on using [Zettlr](https://www.zettlr.com), which is an open source [Markdown](https://en.wikipedia.org/wiki/Markdown) editor that allows me to import my Zotero work and do the appropriate citations. Markdown will allow me to have version control as well as it allowing me to edit my documents anywhere. This also allows me to export my work to pdf, word and many others.
3. For my cloud storage, I am planning on using an open source tool called [GitLab](https://about.gitlab.com) and this allows me to have my work version controlled and accessible anywhere on any device. I have already started using this for my A level notes and work (it is where I keep everything) and it has worked very well so far. It is possible to see my current [EPQ directory here](https://gitlab.com/thomaswholland/epq).
4. As for my time management tools, I use a tool called [Trello](https://trello.com) which allows a board like display of all my tasks and I have used it so far with my A levels and this workflow has been successful thus far.