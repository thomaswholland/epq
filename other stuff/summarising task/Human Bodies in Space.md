# Human Bodies in Space on the way to Mars

One of the most significant problems facing humans in space is the effect of gravitational fields. Humans would have to transition between many different gravitational field strengths on the mission to Mars, which after prolonged times can lead to spatial disorientation as well as a loss of coordination. Whilst travelling, the human would experience zero-gravity, and this would lead to bone density loss of over 1%, and unless dealt with correctly, this could lead to osteoporosis-related fractures later in life. This loss in calcium can also lead to a high chance of developing kidney stones, which develop after dehydration and increased calcium excretion. Also whilst in zero gravity, unless the human body is exercised, the human body could lead to cardiovascular deconditioning and muscle wastage, as there would be a dramatic reduction in the demands that are placed on the human body on a day to day basis.

A significant factor that could cause many problems is the distance from Earth to Mars, 225 million kilometres. The significant distance has several physiological impacts as it could lead to a twenty-minute oneway delay, and so to get a response from Earth, there could be anything up to a 40-minute wait. This could increase the isolation felt and also any urgent time-critical health problems that may occur during the three-year expedition would have to deal with alone with limited supplies.

The mental condition of humans in space also plays a significant role in this as several behavioural issues among confined people in small spaces can develop. Prolonged confinement has been shown to increase depression whilst also inducing boredom if combined with periods that lack purpose or during activities with little reward or have little interest. 

Another mental problem that we have yet been unable to deal with is the effect of the 38 extra minutes in a Mars Sol, and this could throw off the circadian rhythm, and this could lead to sleeping disorders or timing disorientations. 
Physiologically, due to the lack of fresh food, malnutrition, primarily due to a lack of nutrients, could lead to a decrease in cognitive ability and further increase fatigue.
Another problem that would be faced in a closed environment like a spacecraft is that microorganisms can more easily change their characteristics and also be more readily transferred. Also, some problems mentioned earlier, such as fatigue, increased stress and depression can lead to decreased immunity and a reduced ability to fight infections.

One of the most dangerous aspects of having humans in space is the increased exposure of radiation. For example, the ozone layer absorbs 97-99% of all the ultraviolet light, which otherwise causes skin cancer, DNA mutations as well as skin blistering. Other types of radiation that are blocked can also lead to radiation sickness, which can result in short term problems such as nausea but also longer-term problems such as cataracts as well as cardia and circulatory diseases. 

------

*Refrences*

*Main Article:*

*https://www.nasa.gov/sites/default/files/atoms/files/your_body_six_month_in_space_11_18_15_0.pdf*

*Extra Information:*

*http://butane.chem.uiuc.edu/pshapley/GenChem2/A2/1.html*

